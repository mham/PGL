import unittest
import numpy as np
from PGL.components.airfoil import AirfoilShape

class AirfoilShapeTests(unittest.TestCase):
    ''' Tests for components.airfoil.py
    '''
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.af = AirfoilShape()
        self.af.initialize(points=np.loadtxt('data/ffaw3480.dat'))
        self.dps_s01 = np.array([0.0, 0.25, 0.5, 0.75, 1.0])
        
    def test_s_to_11_10(self):
        
        dps_s11 = np.array([self.af.s_to_11(s) for s in self.dps_s01])
        dps_s01n = np.array([self.af.s_to_01(s) for s in dps_s11])
        
        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)
    
    def test_s_to_11_10_rotate(self):
        
        afi = self.af.copy()
        afi.rotate_z(-45.0)
        dps_s11 = np.array([afi.s_to_11(s) for s in self.dps_s01])
        afii = self.af.copy()
        afii.rotate_z(+45.0)
        dps_s01n = np.array([afii.s_to_01(s) for s in dps_s11])
        
        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)
        
    def test_s_to_11_10_scale_equal(self):
        
        afi = self.af.copy()
        afi.scale(1.5)
        points = afi.points
        dps_s11 = np.array([afi.s_to_11(s) for s in self.dps_s01])
        afii = self.af.copy()
        afii.scale(1.5)
        pointsn = afii.points
        dps_s01n = np.array([afii.s_to_01(s) for s in dps_s11])
        
        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)
        self.assertEqual(np.testing.assert_allclose(pointsn, points, 1E-06), None)
        
    def test_s_to_11_10_scale_not_equal(self):
        
        afi = self.af.copy()
        afi.scale(1.1)
        #points = afi.points
        dps_s11 = np.array([afi.s_to_11(s) for s in self.dps_s01])
        afii = self.af.copy()
        afii.scale(1.5)
        #pointsn = afii.points
        dps_s01n = np.array([afii.s_to_01(s) for s in dps_s11])
        
        self.assertEqual(np.testing.assert_allclose(dps_s01n, self.dps_s01, 1E-06), None)
        #self.assertEqual(np.testing.assert_allclose(pointsn, points, 1E-06), None)
        
if __name__ == '__main__':
    
    unittest.main()
