
import unittest
import copy
import numpy as np
from PGL.main.coons import CoonsPatch
from PGL.main.curve import Curve
from PGL.main.bezier import BezierCurve
from PGL.components.airfoil import AirfoilShape, BezierAirfoilShape


def make_surface():

    # create the edges
    sin = np.sin(np.linspace(0, np.pi*2, 20))
    l0 = Curve(points=np.array([np.zeros(20),        np.linspace(0, 2*np.pi,20), np.zeros(20)]).T)
    l1 = Curve(points=np.array([np.ones(20)*np.pi*2, np.linspace(0, 2*np.pi,20), sin]).T)
    l2 = Curve(points=np.array([np.linspace(0, np.pi*2,20), np.zeros(20),      sin]).T)
    l3 = Curve(points=np.array([np.linspace(0, np.pi*2,20), 2*np.pi*np.ones(20), sin]).T)

    # create an instance of the CoonsPatch class
    c = CoonsPatch(20, 20)

    c.add_edge(0, l0)
    c.add_edge(1, l1)
    c.add_edge(2, l2)
    c.add_edge(3, l3)
    c.update()

    return c, l0, l1, l2, l3

def make_bezier_surface():

    # the airfoil shapes
    l0 = BezierCurve()
    l0.CPs = np.array([[1., -.02, 0],
                       [0.7, 0.05, 0],
                       [0.5, -.25, 0],
                       [0.25, -.25, 0],
                       [0, -.2, 0],
                       [0., 0, 0]])
    l0.ni = 50
    l0.update()
    l1 = BezierCurve()
    l1.CPs = np.array([[.7, -.005, 1],
                       [0.65, 0.01, 1.0],
                       [0.5, -.1, 1.0],
                       [0.35, -.1, 1.0],
                       [0.3, -.05, 1.0],
                       [0.3, 0, 1.0]])
    l1.ni = 50
    l1.update()
    l2 = BezierCurve()
    l2.CPs = np.array([[1., -.02, 0],
                       [1., 0., 0.5],
                       [.85, 0, 0.9],
                       [0.7, 0, 1]])
    l2.ni = 20
    l2.update()
    l3 = BezierCurve()
    l3.CPs = np.array([[0., 0, 0],
                       [0., 0., 0.5],
                       [.15, 0, .9],
                       [0.3, 0, 1]])
    l3.ni = 20
    l3.update()
    # create an instance of the CoonsPatch class
    c = CoonsPatch(50, 20)

    c.add_edge(0, l0)
    c.add_edge(1, l1)
    c.add_edge(2, l2)
    c.add_edge(3, l3)
    c.update()

    # the airfoil shapes
    ll0 = BezierCurve()
    ll0.CPs = np.array([[1., .02, 0],
                       [0.7, 0.05, 0],
                       [0.5, .2, 0],
                       [0.25, .2, 0],
                       [0, .1, 0],
                       [0., 0, 0]])
    ll0.ni = 50
    ll0.update()
    ll1 = BezierCurve()
    ll1.CPs = np.array([[.7, .005, 1],
                       [0.65, 0.01, 1.0],
                       [0.5, .1, 1.0],
                       [0.35, .1, 1.0],
                       [0.3, .05, 1.0],
                       [0.3, 0, 1]])
    ll1.ni = 50
    ll1.update()
    ll2 = BezierCurve()
    ll2.CPs = np.array([[1., .02, 0],
                       [1., 0., 0.5],
                       [.85, 0, 0.9],
                       [0.7, 0, 1]])
    ll2.ni = 20
    ll2.update()
    ll3 = BezierCurve()
    ll3.CPs = np.array([[0., 0, 0],
                       [0., 0., 0.5],
                       [.15, 0, .9],
                       [0.3, 0, 1]])
    ll3.ni = 20
    ll3.update()
    # create an instance of the CoonsPatch class
    c1 = CoonsPatch(50, 20)

    c1.add_edge(0, ll0)
    c1.add_edge(1, ll1)
    c1.add_edge(2, ll2)
    c1.add_edge(3, ll3)
    c1.update()
    return c, c1, l0, l1, l2, l3, ll0, ll1, ll2, ll3


def bezier_airfoil_surface():

    # l0 = BezierAirfoilShape()
    l0 = AirfoilShape(points=np.loadtxt('data/ffaw3360.dat'), nd=3)
    # l0.spline_CPs = np.array([0, 0., 0.1, 0.2, 0.4, 0.7,1])
    # l0.fit()
    l0.redistribute(ni=100, even=True)
    l1 = AirfoilShape(points=np.loadtxt('data/ffaw3241.dat'), nd=3)
    l1.redistribute(ni=100, even=True)
    l1.scale(0.5)
    l1.rotate_x(-90.)
    l1.translate_x(0.25)
    l1.translate_z(1)
    l1.translate_y(1)
    l2 = BezierCurve()
    l2.CPs = np.array([l0.points[0],
                       [l0.points[0, 0], l0.points[0, 1], 0.75],
                       [0.75, 0.25, 1.],
                       l1.points[0]])
    l2.ni = 20
    l2.update()
    l3 = BezierCurve()
    l3.CPs = np.array([l0.points[-1],
                       [l0.points[-1, 0], l0.points[-1, 1], 0.75],
                       [0.75, 0.25, 1.],
                       l1.points[-1]])
    l3.ni = 20
    l3.update()
    c = CoonsPatch(100, 20)

    c.add_edge(0, l0)
    c.add_edge(1, l1)
    c.add_edge(2, l2)
    c.add_edge(3, l3)
    c.update()
    return c, l0, l1, l2, l3

from PGL.main.domain import Domain

c, l0, l1, l2, l3 = bezier_airfoil_surface()

c.P.plot_surface_grid()
l0.plot()
l1.plot()
l2.plot()
l3.plot()


# d = Domain()
# d.add_blocks(c.P)
# d.plot_surface_grid()
