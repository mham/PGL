
import numpy as np
import math
import time

from PGL.main.curve import Curve
from PGL.main.coons import CoonsPatch
from PGL.main.domain import Domain, Block
from PGL.main.coons_extrusion import CoonsExtrusion


class CoonsBladeRoot(object):
    """
    Generates a cylindrical root section with an angled junction

    Parameters
    ----------
    nblades: int
        Number of blades
    ds_root_start: float
        spanwise distribution at root start
    ds_root_end: float
        spanwise distribution at root end
    s_root_start: float
        spanwise position of root start
    s_root_end: float
        spanwise position of root start
    ni_root: int
        number of spanwise points
    root_diameter: float
        root diameter
    tip_con: array
        blade connector
    """

    def __init__(self, **kwargs):

        self.nblades = 3
        self.ds_root_start = 0.006
        self.ds_root_end = 0.003
        self.s_root_start = 0.0
        self.s_root_end = 0.05
        self.ni_root = 8
        self.root_diameter = 0.05
        self.tip_con = np.array([])

        for k, w, in kwargs.iteritems():
            if hasattr(self, k):
                setattr(self, k, w)

        self.domain = Domain()

    def update(self):

        t0 = time.time()

        self.root_radius = self.root_diameter / 2.

        self.ni = self.tip_con.shape[0]
        self.nblock = 4
        bsize = (self.ni - 1) / 4

        root_con = np.zeros([self.ni, 3])
        root_con[:,2] = self.s_root_start
        for i in range(self.ni):
            root_con[i, 0] = -self.root_radius*math.cos(360.*i/(self.ni-1)*np.pi/180.)
            root_con[i, 1] = -self.root_radius*math.sin(360.*i/(self.ni-1)*np.pi/180.)
        if self.nblades == 3:
            self.root_angle = np.tan(-np.pi/6.)
            root_con[:bsize+1, 2]          = root_con[:bsize+1, 0]*self.root_angle
            root_con[bsize:2*bsize+1, 2]   = -root_con[bsize:2*bsize+1, 0]*self.root_angle
            root_con[bsize*2:bsize*3+1, 2] = -root_con[2*bsize:3*bsize+1, 0]*self.root_angle
            root_con[bsize*3:bsize*4+1, 2] = root_con[3*bsize:4*bsize+1, 0]*self.root_angle
        elif self.nblades == 4:
            self.root_angle = np.tan(-np.pi/4.)
            root_con[:bsize+1, 2]          = root_con[:bsize+1, 0]*self.root_angle
            root_con[bsize:2*bsize+1, 2]   = -root_con[bsize:2*bsize+1, 0]*self.root_angle
            root_con[bsize*2:bsize*3+1, 2] = -root_con[2*bsize:3*bsize+1, 0]*self.root_angle
            root_con[bsize*3:bsize*4+1, 2] = root_con[3*bsize:4*bsize+1, 0]*self.root_angle
        self.root_con = root_con

        # create surface mesh using the CoonsBladeSection class
        self.surf = CoonsExtrusion(self.root_con, self.tip_con)
        self.surf.interpolant = 'linear'
        self.surf.np = 4
        self.surf.fW0 = 0.5
        self.surf.fW1 = 0.5
        self.surf.ni = self.ni_root
        self.surf.ds0 = self.ds_root_start / ( self.s_root_end - self.s_root_start)
        self.surf.ds1 = self.ds_root_end / ( self.s_root_end - self.s_root_start)

        self.surf.create_section()
        self.surf.setZero(0, 'z')
        self.surf.setZero(1, 'z')
        self.surf.update_patches()
        self.domain = self.surf.domain
        self.domain.rename_block('coons', 'root')

        # self.domain.split_blocks(33)
        print 'root done ...', time.time() - t0
