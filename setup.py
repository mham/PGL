
import sys
import os
import setuptools
from numpy.distutils.core import setup
from numpy.distutils.misc_util import Configuration

def install(package):
    command = ['pip', 'install', '-e', package, '--no-deps --src modules']
    cmd = ' '.join(command)
    out = os.system(cmd)
    print cmd

hypgrid2d = False
pointwiser = False 
if '--with-hypgrid2d' in sys.argv:
    hypgrid2d = True
    sys.argv.remove('--with-hypgrid2d')

# fortran sources
sources = ['src/distfunc.f',
           'src/spline.f',
           'src/pointsort.f']

config = Configuration('PGL')
#config.add_extension('PGLlib', sources=sources, f2py_options=['--fcompiler=intelem', '--compiler=intelem'])
config.add_extension('PGLlib', sources=sources)

kwds = {'install_requires': ['numpy',
                             'scipy',
                             'fortranfile',
                             'sphinx',
                             'sphinxcontrib-napoleon',
                             'sphinx_rtd_theme',
                             ],
        'version': '0.1',
        'zip_safe': False,
        'license':'',
        'packages': ['PGL', 'PGL.components', 'PGL.main'],
        'dependency_links': []
        }

if hypgrid2d:
    install('git+https://gitlab.windenergy.dtu.dk/EllipSys/hypgrid2dwrapper.git#egg=hypgrid2dwrapper')

if pointwiser:
    install('git+https://gitlab.windenergy.dtu.dk/frza/pointwiser.git#egg=pointwiser')
kwds.update(config.todict())

setup(**kwds)
